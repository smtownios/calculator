//
//  ViewController.h
//  CalculatorApp
//
//  Created by JY on 2018. 11. 7..
//  Copyright © 2018년 JY. All rights reserved.
//

#import <UIKit/UIKit.h>

//현재 상태에 대한 열거형을 선언
typedef enum {
    STATUS_DEFAULT=0,
    STATUS_DIVISION,
    STATUS_MULTIPLY,
    STATUS_MINUS,
    STATUS_PLUS,
    STATUS_RETURN
} kStatusCode;


//클래스이름: ViewController
@interface ViewController : UIViewController {
    double curValue;
    double totalCurValue;
    NSString* curInputValue;
    kStatusCode curStatusCode;
}

-(IBAction) digitPressed:(UIButton *)sender;
-(IBAction) operationPressed:(UIButton *)sender;


//@property: getter, setter 메서드 자동 생성해주는 지시어
//.m 파일에 @synthesize 지시어 사용 필요
@property Float64 curValue;
@property Float64 totalCurValue;
@property kStatusCode curStatusCode;
@property (weak, nonatomic) IBOutlet UILabel *displayLabel;


@end
