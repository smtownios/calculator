//
//  main.m
//  CalculatorApp
//
//  Created by JY on 2018. 11. 7..
//  Copyright © 2018년 JY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
