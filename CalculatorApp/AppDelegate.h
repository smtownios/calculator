//
//  AppDelegate.h
//  CalculatorApp
//
//  Created by JY on 2018. 11. 7..
//  Copyright © 2018년 JY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

