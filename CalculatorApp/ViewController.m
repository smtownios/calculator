//
//  ViewController.m
//  CalculatorApp
//
//  Created by JY on 2018. 11. 7..
//  Copyright © 2018년 JY. All rights reserved.
//

#import "ViewController.h"

// *** This is for Class Extension ***
//
//@interface ViewController ()
//
//@end

@implementation ViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self ClearCalculation];
}

// label에 문자열을 출력하는 메서드
-(void)DisplayInputValue:(NSString *)displayText {
    NSString *Comm_displayLabelommaText = [self ConvertComma:displayText];
    [displayLabel setText:CommaText];
}

// 계산결과를 화면에 출력하는 메서드
-(void) DisplayCalculationValue {
    NSString *displayText;
    displayText = [NSString stringWithFormat:@"%g", totalCurValue];
    [self DisplayInputValue:displayText];
    curInputValue = @"";
}

// 계산기를 초기화하는 메서드
-(void) ClearCalculation {
    curInputValue = @"";
    curValue = 0;
    totalCurValue = 0;
    
    [self DisplayInputValue:curInputValue];
    curStatusCode = STATUS_DEFAULT;
}

//천 원 단위를 표시하는 메서드
-(NSString *) ConvertComma: (NSString *)data {
    if (data == nil) return nil;
    if ([data length]<=3) return data;

    NSString *integerString = nil;
    NSString *floatString = nil;
    NSString *minusString = nil;

    NSRange pointRange = [data rangeOfString:@"."];
    if (pointRange.location == NSNotFound) {
        integerString = data;
    } else {
        NSRange r;
        r.location = pointRange.location;
        r.length = [data length] - pointRange.location;
        floatString = [data substringWithRange:r];

        r.location = 0;
        r.length = pointRange.location;
        integerString = [data substringWithRange:r];

    }
    NSRange minusRange = [integerString rangeOfString:@"-"];
    if (minusRange.location != NSNotFound) {
        minusString = @"-";
        integerString = [integerString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }

    NSMutableString *integerStringCommaInserted = [[NSMutableString alloc] init];
    int integerStringLength = [integerString length];
    int idx = 0;
    while (idx < integerStringLength) {
        [integerStringCommaInserted appendFormat:@"%C", [integerString characterAtIndex:idx]];
        if((integerStringLength - (idx + 1)) % 3 == 0 && integerStringLength != (idx + 1)) {
            [integerStringCommaInserted appendString:@","];
        }
        idx ++;
    }
    
    NSMutableString *returnString = [[NSMutableString alloc] init];
    if (minusString != nil) [returnString appendString:minusString];
    if (integerStringCommaInserted != nil) [returnString appendString:integerStringCommaInserted];
    if (floatString != nil) [returnString appendString:floatString];
    
    return returnString;
}

-(IBAction)digitPressed:(UIButton *)sender {
    NSString *numPoint = [[sender titleLabel] text];
    curInputValue = [curInputValue stringByAppendingFormat:numPoint];
    [self DisplayInputValue:curInputValue];
}

-(IBAction)operationPressed:(UIButton *)sender {
    NSString *operationText = [[sender titleLabel] text];
    
    if ([@"+" isEqualToString:operationText]) {
        [self Calculation:curStatusCode CurStatusCode:STATUS_PLUS];
    } else if ([@"-" isEqualToString:operationText]) {
        [self Calculation:curStatusCode CurStatusCode:STATUS_MINUS];
    } else if ([@"x" isEqualToString:operationText]) {
        [self Calculation:curStatusCode CurStatusCode:STATUS_MULTIPLY];
    } else if ([@"/" isEqualToString:operationText]) {
        [self Calculation:curStatusCode CurStatusCode:STATUS_DIVISION];
    } else if ([@"c" isEqualToString:operationText]) {
        [self ClearCalculation];
    } else if ([@"=" isEqualToString: operationText]) {
        [self Calculation:curStatusCode CurStatusCode:STATUS_RETURN];
    }
}

-(void)Calculation:(kStatusCode)StatusCode CurStatusCode:(kStatusCode)cStatusCode {
    switch(StatusCode) {
        case STATUS_DEFAULT:
            [self DefaultCalculation];
            break;
        case STATUS_DIVISION:
            [self DivisionCalculation];
            break;
        case STATUS_PLUS:
            [self PlusCalculation];
            break;
        case STATUS_MULTIPLY:
            [self MultiplyCaculation];
            break;
        case STATUS_MINUS:
            [self MinusCalculation];
            break;
    }
    curStatusCode = cStatusCode;
}

-(void)DefaultCalculation {
    curValue = [curInputValue doubleValue];
    totalCurValue = curValue;
    [self DisplayCalculationValue];
}

-(void)PlusCalculation {
    curValue = [curInputValue doubleValue];
    totalCurValue = totalCurValue + curValue;
    [self DisplayCalculationValue];
}

-(void)MinusCalculation {
    curValue = [curInputValue doubleValue];
    totalCurValue = totalCurValue - curValue;
    [self DisplayCalculationValue];
}

-(void)MultiplyCaculation {
    curValue = [curInputValue doubleValue];
    totalCurValue = totalCurValue * curValue;
    [self DisplayCalculationValue];
}

-(void)DivisionCalculation {
    curValue = [curInputValue doubleValue];
    totalCurValue = totalCurValue / curValue;
    [self DisplayCalculationValue];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
